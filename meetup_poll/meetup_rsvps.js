// This module polls the Meetup RSVP endpoint

// Load date format module
const dateFormat = require('dateformat');

// Load configuration
const config = require('../config/config');

const https = require('https'); // the meetup url seems to require https
const meetupRsvpURL = config.props.meetupRsvpURL
const pollTimeout = config.props.pollTimeout;

// flag to set when poll timeout has been reached so request can be ended
var endRequest = false;

// list of rsvp data collected from stream
var rsvp_info = [];

// Function to poll Meetup RSVPs URL for pollTimeout number of milliseconds
// and accumulate RSVP data
var asyncApiCall = async () => {
  endRequest = false;

  var request = https.get(meetupRsvpURL, function (res) {
    res.on('data', function (chunk) { // called on new RSVPs
      if (endRequest) {
        request.destroy();
        return;
      }

      try {
        var data = JSON.parse(chunk);
        rsvp_info.push(data); // add to rsvp array
      } catch (error) {
        // error handling wasn't specified in requirements
      }
    });
  });

  // function to set endRequest flag to true on timeout
  function handleTimeout() {
    endRequest = true;
  }

  // launcher timer to end request after specified period
  setTimeout(handleTimeout, pollTimeout);
};

// function to tabulate RSVP data
var tabulateRsvpData = () => {
  var tabulatedData = 'none';

  if (endRequest) { // only tabulate data if request has been ended
    // Tabulated data includes:
    // - Total # of RSVPs received
    // - Date of Event furthest into the future
    // - URL for the Event furthest into the future
    // - The top 3 number of RSVPs received per Event host-country

    tabulatedData = rsvp_info.length;

    // get furtherest event in the future
    var evtNdx = -1;
    var ndx = -1;

    // tabulate rsvp data
    rsvp_info.map(rsvp => {
      ndx++;

      if (evtNdx == -1) {
        evtNdx = ndx;
      } else {
        if (rsvp.event.time > rsvp_info[evtNdx].event.time) {
          // save index of event with greater future time
          evtNdx = ndx;
        }
      }
    }) // end map

    // get RSVP with date furtherest in the future
    rsvp = rsvp_info[evtNdx];

    // format date and time (24H format)
    var eventDate = dateFormat(new Date(rsvp.event.time), "yyyy-mm-dd HH:MM");

    // get top 3 event counts per country
    var topThree = getTopThreeRsvpCountries();

    // build tabulated data string
    tabulatedData += ',' + eventDate + ',' + rsvp.event.event_url + ',' + topThree;
  }

  // log tabulated data to console
  if (tabulatedData != 'none') {
    console.log(tabulatedData);
  }

  // return tabulated data to display on screen
  return tabulatedData;
};

// Function to get top 3 number of RSVPs received per Event host-country
function getTopThreeRsvpCountries() {
  // map of countries
  var countries = {}

  // get country counts
  rsvp_info.map(rsvp => {
    var country = rsvp.group.group_country;

    if (country in countries) {
      // increment count
      countries[country]++;
    } else {
      // set initial count for country
      countries[country] = 1;
    }
  }) // end map

  // sort map in descending order - create items array
  var sorted_countries = Object.keys(countries).map(function(key) {
    return [key, countries[key]];
  });

  // sort the array based on the second element
  sorted_countries.sort(function(first, second) {
    return second[1] - first[1];
  });
  
  // get top 3 countries
  var topThree = sorted_countries.slice(0, 3);

  // return results
  return topThree;
}

// exported functions
module.exports = { asyncApiCall, tabulateRsvpData };