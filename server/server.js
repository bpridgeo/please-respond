// This module serves up the web page
const http = require('http');
const fileSystem = require('fs');
const path = require('path');

// Load Meetup RSVP Poll module
const meetupPoller = require('../meetup_poll/meetup_rsvps');

// Load configuration
const config = require('../config/config');

// get configuration values
const hostname = config.props.server_ip;
const port = config.props.server_port;
const pageRefresh = config.props.pageRefresh;

// refresh counter
var itr = 0;

// flag to see if Meetup Poller started
var pollerStarted = false;

// Function to start web server
var startServer = () => {
  var server = http.createServer((req, res) => {
    // Start Meetup Poller on first page hit
    if (!pollerStarted) {
      // Start Meetup RSVP poller
      meetupPoller.asyncApiCall();

      pollerStarted = true;
    }

    itr++;

    // load and stream view html pase to response
    var filePath = path.join(__dirname, '../view/index.html');
    let stream = fileSystem.createReadStream(filePath);
    var view = '';

    stream.on('data', function (data) {
      view += data.toString();
    });

    stream.on('end', function () {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write(getTabulatedRSVPs(view)); // replace $data$ token with poll message or tabulated data
      res.end();
    });
  });

  server.listen(port, hostname, () => {
    pollerStarted = false;
    console.log(`Server running at http://${hostname}:${port}/`);
  });
};

// Function to get tabulated results from Meetup RSVP Poller
function getTabulatedRSVPs(view) {
  var tabulatedRSVPs = meetupPoller.tabulateRsvpData();
  var string = '';

  // See if request is ended
  if (tabulatedRSVPs == 'none') {
    // give waiting message
    var msg = 'Polling for Meetup RSVPs ';
    for (var i=0; i < itr; i++) msg += '.';

    string = view.replace('$interval$', pageRefresh).replace('$data$', msg);
  } else {
    // display tabluated RSVP data
    string = view.replace('$interval$', -1).replace('$data$', tabulatedRSVPs);

    // setup for restart of polling after a page refresh
    pollerStarted = false;
    itr = 0;
  }

  return string;
}

// exported functions
module.exports = { startServer };
