// Entry point for application

// Load Server module
const server = require('./server/server');

// Start server
server.startServer();